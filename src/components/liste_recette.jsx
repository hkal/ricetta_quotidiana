import React ,{ useState, useEffect }from "react";
// import { useAsync } from "react-async";
import { Page, Navbar, BlockTitle, List, ListItem, Checkbox } from 'framework7-react';
import ArticleRecette from "../components/recette_article";
import "../css/recette_article.css";
import "../css/liste_recette.css";
import { click } from "dom7";



const ListeRecette = ({data}) => {
  const [filter, setFilter] = useState("");
  const [OnlyFavorie, setOnlyFavorie] = useState(false);
  const [RecetteFavorie, setRecetteFavorie] = useState(undefined);
  const [IP,setIP] = useState(undefined);

  console.log("test localstorage");

  // useEffect(() => {
  //   if (!IP) {
  //     fetch('https://geolocation-db.com/json/')
  //       .then((resp) => resp.json())
  //       .then((json) => setIP(json.IPv4));
  //   };
  // });

  useEffect(() => {
    if (!RecetteFavorie) {
      var favorie = localStorage.getItem('myFavorie');
      favorie = JSON.parse(favorie);
      if(!favorie){
        var favorie = [];
        localStorage.setItem('myFavorie', JSON.stringify(favorie));
      }
      setRecetteFavorie([...favorie])
    };
  });

  const onChangeFavorie = ()=>{
    var favorie = localStorage.getItem('myFavorie');
    favorie = JSON.parse(favorie);
    if(!favorie){
      var favorie = [];
    }
    localStorage.setItem('myFavorie', JSON.stringify(favorie));
    setRecetteFavorie([...favorie])
  };


  console.log("Liste recette component");
  // console.log(data);
  // console.log(search_url);
  console.log(RecetteFavorie)
  if(data && RecetteFavorie){
    return(
    <div>
      <div className="liste_recette_filter_input">
      <input 
        type="search"
        placeholder="Filtrer"
        onChange={(event) => setFilter(event.target.value)}
      ></input>
      <div className="liste_recette_checkbox">
      <i className="f7-icons">heart_fill</i> 
      {/* <Checkbox name="demo-radio-inline" title="Inclure uniquement mes recettes favories" value="inline-1" checked={OnlyFavorie} onChange={(e) => setOnlyFavorie(e.target.checked)}/> */}
      <ListItem checkbox title="Keep only" value="inline-1" checked={OnlyFavorie} onChange={(e) => setOnlyFavorie(e.target.checked)}/>
      </div>
      </div>
      <h1 style={{"color": "#077354","z-index": "300"}}> {data.recettes.filter(
        (recette) => (!filter || recette.nom.includes(filter)) & ((RecetteFavorie.includes(recette.url) & OnlyFavorie) || !OnlyFavorie)
      ).length} Recette(s)</h1>
      <ul className="recipe_results">
      {data.recettes.filter(
        (recette) => (!filter || recette.nom.includes(filter)) & ((RecetteFavorie.includes(recette.url) & OnlyFavorie) || !OnlyFavorie)
      ).map((recette,index) => (
       <li  key={index}> <ArticleRecette recette={recette} index={index} onChangeFavorie={onChangeFavorie} isFavorie={RecetteFavorie.includes(recette.url)}/></li>
      ))}
      </ul>
    </div>
    );
  }
  return(<div>Chargement en cours ...</div>);
};


export default ListeRecette;