import React,{useState,useEffect} from "react";

const Heart = ({ recette,active,onChangeFavorie}) => {

  const [isActive, setActive] = useState(undefined);

  const toggleClass = () => {
    console.log("toggle class heart 0")
    setActive(!isActive);
    var favorie = localStorage.getItem('myFavorie');
    favorie = JSON.parse(favorie);
    if(!favorie){
      var favorie = [];
    }

    if(favorie.includes(recette.url)){
      favorie.splice(favorie.indexOf(recette.url), 1);
    } else{
      favorie.push(recette.url);
    }
    console.log("tooglClasse Heart")
    console.log(favorie)
    localStorage.setItem('myFavorie', JSON.stringify(favorie));
  };

  useEffect(() => {
      // if (!isActive) {
        var favorie = localStorage.getItem('myFavorie');
        favorie = JSON.parse(favorie);
        console.log("useeffect Heart");
        console.log(favorie);
        console.log(recette.url)
        console.log("fin useffect heart")
        if(!favorie){
          var favorie = [];
          localStorage.setItem('myFavorie', JSON.stringify(favorie));
        }
        setActive(favorie.includes(recette.url))
      // };
  });
  
  return (
    <div className="inhalt">
    <svg className={isActive ? 'one on': 'one'}  viewBox="0 0 100 100" onClick={() => {toggleClass();onChangeFavorie()}}>
      <g className="heartOne">
      <path  className="heartEX" d="M 90,40 a 20 20 0 1 0 -40,-25 a 20 20 0 1 0 -40,25 l 40,50  z" />
      <path className="heart"    d="M 90,40 a 20 20 0 1 0 -40,-25 a 20 20 0 1 0 -40,25 l 40,50  z" />
        </g>
    </svg>
    </div>
  )
};

export default Heart;
