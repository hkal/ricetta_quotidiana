import React, { useState, useEffect } from 'react';
import { getDevice }  from 'framework7/lite-bundle';
import {
  f7,
  f7ready,
  App,
  Panel,
  Views,
  View,
  Popup,
  Page,
  Navbar,
  Toolbar,
  NavRight,
  Link,
  Block,
  BlockTitle,
  LoginScreen,
  LoginScreenTitle,
  List,
  ListItem,
  ListInput,
  ListButton,
  BlockFooter
} from 'framework7-react';
import axios from 'axios';

import capacitorApp from '../js/capacitor-app';
import routes from '../js/routes';
import store from '../js/store';





const MyApp = () => {
  console.log(Date())
  // Login screen demo data
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const device = getDevice();
  console.log(device);

  //creating IP state
  const [ip,setIP] = useState(undefined);

  //creating function to load ip address from the API
  useEffect(() => {
    if (!ip) {
      fetch('https://geolocation-db.com/json/')
        .then((resp) => resp.json())
        .then((json) => setIP(json.IPv4));
    };
  });
  console.log(ip);

  // User Profile
  var UserProfile = localStorage.getItem('UserProfile');
  UserProfile = JSON.parse(UserProfile);
  if(!UserProfile){
    var UserProfile = {"nom":"Vador",
    "prenom":"Dark",
    "adresse":"40 rue Etoile de la Mort, 35000 Tatooine",
    "email":"jesuistonpere@gmail.com",
     "tel":"02.35.71.11.31",
     "max_result":50
    };
  }
  localStorage.setItem('UserProfile', JSON.stringify(UserProfile));

  // UserSearch Param
  var SearchParams = localStorage.getItem('mySearchParams');
  SearchParams = JSON.parse(SearchParams);
  if(!SearchParams){
    var SearchParams = {
      "type_de_plat":{
          "url_para":"dt",
          "values":{"entree":false,
              "platprincipal":false,
              "dessert":false,
              "amusegueule":false,
              "accompagnement":false,
              "sauce":false,
              "boisson":true,
              "confiserie":false}
      },
      "difficulte":{
          "url_para":"dif",
          "values":{"1":true,
              "2":true,
              "3":true,
              "4":true}
      },
      "cout":{
          "url_para":"exp",
          "values":{"1":true,
              "2":true,
              "3":true}
      },
      "regime":{
          "url_para":"prt",
          "values":{"1":false,
              "2":false,
              "3":false,
              "4":true}
      },
      "materiel":{
          "url_para":"rct",
          "values":{"1":true,
              "2":true,
              "3":true,
              "4":true,
              "5":true}
      },
      "temps_de_cuisine":{
          "url_para":"ttlt",
          "values":{"15":true,
              "30":true,
              "45":true}
      }
    };
    localStorage.setItem('mySearchParams', JSON.stringify(SearchParams));
  }

  // Framework7 Parameters
  const f7params = {
    name: 'Ricetta Quotidiana', // App name
      theme: 'auto', // Automatic theme detection


      id: 'io.framework7.myapp', // App bundle ID
      // App store
      store: store,
      // App routes
      routes: routes,

      // Input settings
      input: {
        scrollIntoViewOnFocus: device.capacitor,
        scrollIntoViewCentered: device.capacitor,
      },
      // Capacitor Statusbar settings
      statusbar: {
        iosOverlaysWebView: true,
        androidOverlaysWebView: false,
      },
  };

  f7ready(() => {

    // Init capacitor APIs (see capacitor-app.js)
    if (f7.device.capacitor) {
      capacitorApp.init(f7);
    }
    // Call F7 APIs here
  });

  return (
    <App { ...f7params } >
      
        {/* Views/Tabs container */}
        <Views tabs className="safe-areas">

          {/* Your main view/tab, should have "view-main" class. It also has "tabActive" prop */}
          <View id="view-home" main tab  tabActive url={"/?ip="+ip} />

          {/* Settings View */}
          <View id="view-settings" name="settings" tab url="/settings/" />

        </Views>

      
    </App>
  )
}
export default MyApp;