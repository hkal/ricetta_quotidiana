import React, { useState } from "react";
import 'framework7-icons';
import { Button } from "framework7-react";
import Heart from "../components/Heart";
import "../css/Heart.css";


const ArticleRecette = ({ recette,index,onChangeFavorie,isFavorie2}) => {
    const [isFavorie, ] = useState(index%2===0 ? 0:1);

    const [Star1, ] = useState(recette.note>=1 ? 0:1);
    const [Star2, ] = useState(recette.note>=2  ? 0:1);
    const [Star3, ] = useState(recette.note>=3  ? 0:1);
    const [Star4, ] = useState(recette.note>=4  ? 0:1);
    const [Star5, ] = useState(recette.note>=5  ? 0:1);

    console.log("Article recette component");
    console.log(recette.url);
    console.log(isFavorie2);


  return (

<div>  
{/* <Button name={index} onClick={(e) => (console.log("jkj"+e.value))}>Ajouter Favorie</Button> */}
{/* <Heart recette={recette} onChangeFavorie={onChangeFavorie} active={isFavorie2}></Heart>   href={"/detail_recette/?recette_url="+recette.url} */}
<div className={"box parent-recipe-article paire" + index%3}>
    <a className="image-recipe-article"  href={"/detail_recette/?recette_url="+recette.url+"&recette_nom="+recette.nom}> 
        <img src={recette.img.width["1200w"]} alt="" className="image-recipe-article-child"></img>
    </a>
    <div className="main-recipe-article">
        <a className="title-recipe-article"   href={"/detail_recette/?recette_url="+recette.url+"&recette_nom="+recette.nom}> 
            <span >{recette.nom}</span>
            
            <br></br>
        </a>
        
        <div className="footer-recipe-article">
         <span className={"heart heart"+isFavorie}> 
         {/* <i className="f7-icons">heart_fill</i> */}
         <Heart recette={recette} onChangeFavorie={onChangeFavorie} active={isFavorie2}></Heart>
         </span>
         
         <a className="star"  href={"/detail_recette/?recette_url="+recette.url+"&recette_nom="+recette.nom}>
            <span className={"star"+Star1}><i className="f7-icons">star_fill</i></span> 
            <span className={"star"+Star2}><i className="f7-icons">star_fill</i></span> 
            <span className={"star"+Star3}><i className="f7-icons">star_fill</i></span> 
            <span className={"star"+Star4}><i className="f7-icons">star_fill</i></span> 
            <span className={"star"+Star5}><i className="f7-icons">star_fill</i></span> 
            <br></br>
            <span className="note-star star0">{recette.note}/5</span>
         </a>
        </div>
    </div>

</div>
</div>    
  )
};

export default ArticleRecette;
