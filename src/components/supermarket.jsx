import React, { useState,useEffect } from "react";
import { useCoordinate} from '../hooks/useCoordinate'; 



const SupermarketComp = () => {
    const [SuperMarket, setSuperMarket] = useState(undefined);
    const [Localisation, setLocalisation] = useState(undefined);
    const {Coordinates,findCoordinates } = useCoordinate();

    findCoordinates();



      const FindlocationPermission = async () => {
        if (!Localisation) {
            try{
                console.log('Current position:', Coordinates);
            } catch{
                console.log("echec")
            }
          fetch('https://geolocation-db.com/json/')
          .then((resp) => resp.json())
          .then((json) => setLocalisation({...{"latitude":json.latitude,"longitude":json.longitude}}));
        }
    }

    useEffect(() => {
        console.log("totom")
        console.log(Localisation)
        var tomtom_url = "https://api.tomtom.com/search/2/search/supermarkets.json?&radius=2000&minFuzzyLevel=1&maxFuzzyLevel=2&view=Unified&relatedPois=off&key=cOhAydGUP6BZTjcsMzCN5iJzdiLS5yXU"
          if (!SuperMarket) {
            FindlocationPermission();
              if(Localisation){
                console.log("totom fetch")
                var tomtom_url2 = tomtom_url + "&lat="+ Localisation.latitude +"&lon="+Localisation.longitude
                // var tomtom_url2 = tomtom_url + "&lat=48.050977" +"&lon=-1.742526"

                fetch(tomtom_url2)
                    .then((resp) => resp.json())
                    .then((json) => setSuperMarket(json));
              }
           
          }
    });
if(SuperMarket){
    return (

        <div>  
        <h2 className='title_detail_recette'>Besoin de faire des courses ?</h2>
              <h4>Voici une liste de supermaché proche de ta localisation dans un rayon de 2km</h4>
              <ul>
              {SuperMarket.results.map((supermarket) => ( <li> {supermarket.poi.name}: {supermarket.address.freeformAddress}</li>))}
              </ul>
        </div>    
          )
} else{
    return(<div></div>)
}

};

export default SupermarketComp;
