import React,{ useState, useEffect } from 'react';
import {
  Page,
  Panel,
  Popup,
  Navbar,
  NavLeft,
  NavTitle,
  NavTitleLarge,
  NavRight,
  Link,
  Toolbar,
  Block,
  BlockTitle,
  List,
  ListItem,
  Row,
  Stepper,
  Col,
  Button,
  View,
  AccordionContent,
  AccordionItem,
  AccordionToggle,
  Checkbox,
  ListInput,
  Icon
} from 'framework7-react';

import "../css/setting.css";


const SettingPage = (props) => {
  const { f7route, f7router } = props;
  // const UserProfile = JSON.parse(localStorage.getItem('UserProfile'));
  // const mySearchParams = JSON.parse(localStorage.getItem('mySearchParams'));
  const [mySearchParams,setmySearchParams] = useState(JSON.parse(localStorage.getItem('mySearchParams')));
  const [UserProfile,setUserProfile] = useState(JSON.parse(localStorage.getItem('UserProfile')));



  const onSearchParamsChange= (e) => {
    mySearchParams[e.target.name]["values"][e.target.value] = e.target.checked;
    setmySearchParams({...mySearchParams});
  };

  const onUserProfileChange= (e) => {
    UserProfile[e.target.name] = e.target.value;
    setUserProfile({...UserProfile});
  };

  console.log("Setting")
  console.log(mySearchParams)


  console.log("UserProfile")
  console.log(UserProfile)


  if(true){
    return(
      <Page title="Settings" name="setting">
        <Navbar  backLink="Retour aux Résultats" />
        <Block>
          <div><h3>Mon Profil</h3></div>
        <List noHairlinesMd>
              <ListInput
                label="Nom"
                type="text"
                name='nom'
                placeholder={UserProfile.nom}
                clearButton
                onChange={(e) => (onUserProfileChange(e))}
              ></ListInput>
              <ListInput
                label="Prénom"
                type="text"
                name='prenom'
                placeholder={UserProfile.prenom}
                clearButton
                onChange={(e) => (onUserProfileChange(e))}
              ></ListInput>
              <ListInput
                label="E-mail"
                type="email"
                name='email'
                placeholder={UserProfile.email}
                validate
                clearButton
                onChange={(e) => (onUserProfileChange(e))}
              ></ListInput>
              <ListInput
                label="Adresse"
                type="text"
                name='adresse'
                placeholder={UserProfile.adresse}
                clearButton
                onChange={(e) => (onUserProfileChange(e))}
              ></ListInput>
              <ListInput
                label="Numéro de téléphone"
                type="tel"
                name='tel'
                placeholder={UserProfile.tel}
                validate
                clearButton
                onChange={(e) => (onUserProfileChange(e))}
              ></ListInput>

              <Button onClick={() => (localStorage.setItem('UserProfile', JSON.stringify(UserProfile)))}>Sauvegarder</Button>
            </List>
        </Block>
        <Block>
        {/* Parametres preference */}
        <div><h3>Paramètres de recherches par défaut</h3></div> 
        <List accordionList>
                {/* Type de plat */}
                 <ListItem accordionItem title="Type de plat">
                   <AccordionContent>
                     <Block>
                       <List>
                         <ListItem checkbox title="Entrée" name="type_de_plat" value="entree" defaultChecked={mySearchParams.type_de_plat.values.entree} onChange={(e) => onSearchParamsChange(e)}/>
                         <ListItem checkbox title="Plat" name="type_de_plat" value="platprincipal" defaultChecked={mySearchParams.type_de_plat.values.platprincipal} onChange={(e) => onSearchParamsChange(e)}/>
                         <ListItem checkbox title="Dessert" name="type_de_plat" value="dessert" defaultChecked={mySearchParams.type_de_plat.values.dessert} onChange={(e) => onSearchParamsChange(e)}/>
                         <ListItem checkbox title="Amuse-Gueule" name="type_de_plat" value="amusegueule" defaultChecked={mySearchParams.type_de_plat.values.amusegueule} onChange={(e) => onSearchParamsChange(e)}/>
                         <ListItem checkbox title="Accompagnement" name="type_de_plat" value="accompagnement" defaultChecked={mySearchParams.type_de_plat.values.accompagnement} onChange={(e) => onSearchParamsChange(e)}/>
                         <ListItem checkbox title="Sauce" name="type_de_plat" value="sauce" defaultChecked={mySearchParams.type_de_plat.values.sauce} onChange={(e) => onSearchParamsChange(e)}/>
                         <ListItem checkbox title="Boisson" name="type_de_plat" value="boisson" defaultChecked={mySearchParams.type_de_plat.values.boisson} onChange={(e) => onSearchParamsChange(e)}/>
                         <ListItem checkbox title="Confiserie" name="type_de_plat" value="confiserie" defaultChecked={mySearchParams.type_de_plat.values.confiserie} onChange={(e) => onSearchParamsChange(e)}/>
                       </List>
                    </Block>
                  </AccordionContent>
                </ListItem>

                     {/* Difficulte */}
                     <ListItem accordionItem title="Difficultés">
                   <AccordionContent>
                     <Block>                       
                       <List>
                         <ListItem checkbox title="Très Facile" name="difficulte" value="1" defaultChecked={mySearchParams.difficulte.values["1"]} onChange={(e) => onSearchParamsChange(e)}/>
                         <ListItem checkbox title="Facile" name="difficulte" value="2" defaultChecked={mySearchParams.difficulte.values["2"]} onChange={(e) => onSearchParamsChange(e)}/>
                         <ListItem checkbox title="Moyen" name="difficulte" value="3" defaultChecked={mySearchParams.difficulte.values["3"]} onChange={(e) => onSearchParamsChange(e)}/>
                         <ListItem checkbox title="Difficile" name="difficulte" value="4" defaultChecked={mySearchParams.difficulte.values["4"]} onChange={(e) => onSearchParamsChange(e)}/>
                       </List>
                    </Block>
                  </AccordionContent>
                </ListItem>

                {/*Cout */}
                <ListItem accordionItem title="Coût">
                   <AccordionContent>
                     <Block>                       
                       <List>
                         <ListItem checkbox title="Bon Marché" name="cout" value="1" defaultChecked={mySearchParams.cout.values["1"]} onChange={(e) => onSearchParamsChange(e)}/>
                         <ListItem checkbox title="Coût Moyen" name="cout" value="2" defaultChecked={mySearchParams.cout.values["2"]} onChange={(e) => onSearchParamsChange(e)}/>
                         <ListItem checkbox title="Assez cher" name="cout" value="3" defaultChecked={mySearchParams.cout.values["3"]} onChange={(e) => onSearchParamsChange(e)}/>
                       </List>
                    </Block>
                  </AccordionContent>
                </ListItem>              

                {/* Regime */}
                <ListItem accordionItem title="Régime Alimentair">
                   <AccordionContent>
                     <Block>                       
                       <List>
                         <ListItem checkbox title="Végétarien" name="regime" value="1" defaultChecked={mySearchParams.regime.values["1"]} onChange={(e) => onSearchParamsChange(e)}/>
                         <ListItem checkbox title="Sans gluten" name="regime" value="2" defaultChecked={mySearchParams.regime.values["2"]} onChange={(e) => onSearchParamsChange(e)}/>
                         <ListItem checkbox title="Végan" name="regime" value="3" defaultChecked={mySearchParams.regime.values["3"]}onChange={(e) => onSearchParamsChange(e)}/>
                         <ListItem checkbox title="Sans Lactose" name="regime" value="4" defaultChecked={mySearchParams.regime.values["4"]} onChange={(e) => onSearchParamsChange(e)}/>
                       </List>
                    </Block>
                  </AccordionContent>
                </ListItem>   

                {/* Materiel */}
                <ListItem accordionItem title="Matériel">
                   <AccordionContent>
                     <Block>                       
                       <List>
                         <ListItem checkbox title="Four" name="materiel" value="1" defaultChecked={mySearchParams.materiel.values["1"]}  onChange={(e) => onSearchParamsChange(e)}/>
                         <ListItem checkbox title="Plaques" name="materiel" value="2" defaultChecked={mySearchParams.materiel.values["2"]}  onChange={(e) => onSearchParamsChange(e)}/>
                         <ListItem checkbox title="Sans Cuisson" name="materiel" value="3" defaultChecked={mySearchParams.materiel.values["3"]}  onChange={(e) => onSearchParamsChange(e)}/>
                         <ListItem checkbox title="Micro-ondes" name="materiel" value="4" defaultChecked={mySearchParams.materiel.values["4"]}  onChange={(e) => onSearchParamsChange(e)}/>
                         <ListItem checkbox title="Barbecue/Plancha" name="materiel" value="5" defaultChecked={mySearchParams.materiel.values["5"]}  onChange={(e) => onSearchParamsChange(e)}/>
                       </List>
                    </Block>
                  </AccordionContent>
                </ListItem>   

                {/* Temps */}
                <ListItem accordionItem title="Temps total">
                   <AccordionContent>
                     <Block>                       
                       <List>
                         <ListItem checkbox title="Moins de 15min" name="temps_de_cuisine" value="15" defaultChecked={mySearchParams.temps_de_cuisine.values["15"]} onChange={(e) => onSearchParamsChange(e)}/>
                         <ListItem checkbox title="Moins de 30min" name="temps_de_cuisine" value="30" defaultChecked={mySearchParams.temps_de_cuisine.values["30"]} onChange={(e) => onSearchParamsChange(e)}/>
                         <ListItem checkbox title="Moins de 45min" name="temps_de_cuisine" value="45" defaultChecked={mySearchParams.temps_de_cuisine.values["45"]} onChange={(e) => onSearchParamsChange(e)}/>
                       </List>
                    </Block>
                  </AccordionContent>
                </ListItem>        


              <Button onClick={() => (localStorage.setItem('mySearchParams', JSON.stringify(mySearchParams)))}>Sauvegarder</Button>
          </List>                               
          </Block>
      </Page>
    );
  }
  return(<Page title="Settings" name="detail_recette">
  <Navbar  backLink="Retour à l'Accueil" />
  Chargement en cours ...
  </Page>
  );
}
export default SettingPage;