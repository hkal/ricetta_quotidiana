import React,{ useState, useEffect } from 'react';
import {
  Page,
  Panel,
  Popup,
  Navbar,
  NavLeft,
  NavTitle,
  NavTitleLarge,
  NavRight,
  Link,
  Toolbar,
  Block,
  BlockTitle,
  List,
  ListItem,
  Row,
  Stepper,
  Col,
  Button,
  View,
  AccordionContent,
  AccordionItem,
  AccordionToggle,
  Checkbox,
  Icon,
  ListInput
} from 'framework7-react';
import "../css/detail_recette.css"
import { usePhotoGallery} from '../hooks/usePhotoGallery'; 
import SupermarketComp from '../components/supermarket';

const DetailRecette = (props) => {
  console.log("detail recette")
    const { f7route, f7router } = props;
    const [DataDetailRecette, setDataDetailRecette] = useState(undefined);
    const {photos,takePhoto } = usePhotoGallery();
    const [NomRecette,setNomRecette] = useState("");
    const UserProfile  = JSON.parse(localStorage.getItem('UserProfile'));
    const [InvitatParam,setInvitatParam] = useState({...{"mail_invite":"","date_invite":"05/05/2022, 18:30"},...JSON.parse(localStorage.getItem('UserProfile'))} );
    
    console.log("test")
    console.log(InvitatParam)


    useEffect(() => {
      console.log("Add title")
      console.log(f7route.query)
        if (!DataDetailRecette && "recette_url" in f7route.query) {
        fetch("https://ricetta-quotidiana-api.vercel.app/api/v1/recette?url="+f7route.query["recette_url"])
            .then((resp) => resp.json())
            .then((json) => setDataDetailRecette(json));
        setNomRecette(f7route.query["recette_nom"]);
        }
  });

  const onInviteChange= (e) => {
    InvitatParam[e.target.name] = e.target.value;
    setInvitatParam({...InvitatParam});
  };

  const ValideInvite= () => {
    console.log("valider inviter")
    var url = "https://ricetta-quotidiana-api.vercel.app/api/v1/invitation?";
    // console.log(InvitatParam.map((value,key) => (console.log(value+" "+key)) ));
    for (var key in InvitatParam) {
      url = url + "&" + key+"="+InvitatParam[key]
    }
    url = url + "&image=" + DataDetailRecette.image["5"]+"&nom_plat="+NomRecette
    console.log(url)
    console.log("envoie invitation")
    fetch(url)
    .then((resp) => resp.json())
    .then((json) => console.log(json));

    // "https://ricetta-quotidiana-api.vercel.app/api/v1/invitation?mail_invite=hugo.miccinilli@gmail.com;hugo.miccinilli@eleve.ensai.fr;id1896@ensai.fr&nom=boss&prenom=hugo&date_invite=06/08/1998&adresse=etoile%20de%20la%20mort&tel=0666565545&email=test@gmail.com&nom_plat=lasagne&image=https://assets.afcdn.com/recipe/20190529/93191_w96h96c1cx4330cy2886cxb8660cyb5773.jpg"
  };

  
  if(DataDetailRecette){
    return(
      <Page title={DataDetailRecette.recipeCategory} name="detail_recette">
        <Navbar  backLink="Retour aux Résultats" />
        <br></br>
        
             <div><img src={DataDetailRecette.image["5"]} alt="" className="image-details"></img>
             {/* <Link iconIos="f7:camera_fill" iconAurora="f7:camera_fill" onClick={() => takePhoto()} className="take_photo_icon" iconSize={50}/> */}
             <Button onClick={() => takePhoto()} className="take_photo_icon"> <Icon f7="camera_fill" iconSize="100px" className="take_photo_icon"></Icon></Button>
             </div>
             <br></br><br></br>

             <h2 className='main_info_detail_recette'>{NomRecette}</h2>
             <h3 className='main_info_detail_recette'> <Icon f7="timer_fill"></Icon>  {DataDetailRecette.temps} <Icon f7="staroflife_fill"></Icon> {DataDetailRecette.difficulte} <Icon f7="money_euro_circle_fill"></Icon> {DataDetailRecette.cout}</h3>
            
            
           {/* Popup Menu aleatoire */}
    <Popup id="popup-invitation">
        <View>
          <Page>
            <Navbar title="Envoyer des invitations">
              <NavRight>
                <Link popupClose>Close</Link>
              </NavRight>
            </Navbar>
            <Block>
              <h1 style={{"color":"#077354","text-align": "center"}}>Invitations</h1>
              <List noHairlinesMd>
              <ListInput
                label="Mails de mes invités (à séparer par ';')"
                type="text"
                name='mail_invite'
                placeholder="invite1@gmail.com;invite2@hotmail.fr"
                clearButton
                onChange={(e) => (onInviteChange(e))}
              ></ListInput>
              <ListInput
              label="Date et heure du repas (MM/DD/YYYY, HH:MM:(A/M))"
              type="datetime-local"
              name="date_invite"
              defaultValue={InvitatParam.date_invite}
              onChange={(e) => (onInviteChange(e))}
               ></ListInput>
              </List>

              <h1 style={{"color":"#077354","text-align": "center"}}>Mes coordonnées</h1>
              <List accordionList>
                {/* Type de plat */}
                 <ListItem accordionItem title="">
                   <AccordionContent>
                     <Block>
                       <List>
                       <ListInput
                label="Nom"
                type="text"
                name='nom'
                defaultValue={UserProfile.nom}
                clearButton
                onChange={(e) => (onInviteChange(e))}
              ></ListInput>
              <ListInput
                label="Prénom"
                type="text"
                name='prenom'
                defaultValue={UserProfile.prenom}
                clearButton
                onChange={(e) => (onInviteChange(e))}
              ></ListInput>
              <ListInput
                label="E-mail"
                type="email"
                name='email'
                defaultValue={UserProfile.email}
                validate
                clearButton
                onChange={(e) => (onInviteChange(e))}
              ></ListInput>
              <ListInput
                label="Adresse"
                type="text"
                name='adresse'
                defaultValue={UserProfile.adresse}
                clearButton
                onChange={(e) => (onInviteChange(e))}
              ></ListInput>
              <ListInput
                label="Numéro de téléphone"
                type="tel"
                name='tel'
                defaultValue={UserProfile.tel}
                validate
                clearButton
                onChange={(e) => (onInviteChange(e))}
              ></ListInput>
                       </List>
                    </Block>
                  </AccordionContent>
                </ListItem>
                </List>
              
              
              <Button fill raised popupClose onClick={()=>ValideInvite()}>Envoyer</Button>
            </Block>
          </Page>
        </View>
    </Popup>

      <Block strong className='liste_detail_recette'>
    {/* Ingredient */}
    <List className='liste_detail_recette'>
      <h1 className='title_detail_recette'>Ingredient</h1>
      
      {DataDetailRecette.recipeIngredient.map((ingredient,index) => (
        <ListItem className='liste_detail_recette' title={"-"+ingredient}/>
      ))}
    </List >
      {/* Etape */}
      <List className='liste_detail_recette'>
      <h1 className='title_detail_recette'>Instruction</h1>
      {DataDetailRecette.recipeInstructions.map((step,index) => (
        <ListItem className='liste_detail_recette' title={(index+1)+": "+step}/>
      ))}
      <Button className='button_invitation' fill raised  popupOpen="#popup-invitation">Inviter des amis</Button>
      </List>
      <h1 className='title_detail_recette'>Mes réalisations</h1>
      {photos.map((photo) => ( <img src={photo.webviewPath} className="image-details"></img>))}

      <SupermarketComp></SupermarketComp>
      </Block>
    </Page>
    );
  }
  return(<Page>
    Chargement en cours ...
    <Block strong>
        <Link onClick={() => f7router.navigate("/?search_url="+f7route.query["search_url"])}>Retour aux Résultats</Link>
      </Block>
    </Page>);

}
export default DetailRecette;