import React,{ useState, useEffect } from 'react';
import {
  Page,
  Panel,
  Popup,
  Navbar,
  NavLeft,
  Subnavbar,
  NavTitle,
  NavTitleLarge,
  NavRight,
  Link,
  Toolbar,
  Block,
  BlockTitle,
  List,
  ListItem,
  Row,
  Stepper,
  Col,
  Button,
  View,
  AccordionContent,
  AccordionItem,
  AccordionToggle,
  Checkbox
} from 'framework7-react';
import ListeRecette from '../components/liste_recette';
import "../css/home.css"
import DetailRecette from "./detail_recette.jsx"

const HomePage = (props) => {
  const { f7route, f7router } = props;

  const [data, setData] = useState(undefined);
  const [NbRandMenu,setNbRandMenu] = useState(1);
  const [SearchUrl,setSearchUrl] = useState("");
  const [UserData, setUserData] = useState(undefined);
  const [InitParam,setInitParam] = useState(true);
  const [InitSearchUrl,setInitSearchUrl] = useState("");

  const [TypePlat,setTypePlat] = useState([]);
  const [Difficulte,setDifficulte] = useState([]);
  const [Cout,setCout] = useState([]);
  const [Regime,setRegime] = useState([]);
  const [Temps,setTemps] = useState([]);
  const [Materiel,setMateriel] = useState([]);

  console.log("ip home");
  console.log(f7route.query)

  useEffect(() => {
    if (!UserData | InitParam) {
      var SearchParams = localStorage.getItem('mySearchParams');
      SearchParams = JSON.parse(SearchParams);
      if(!SearchParams){
        var SearchParams = {
          "type_de_plat":{
              "url_para":"dt",
              "values":{"entree":false,
                  "platprincipal":false,
                  "dessert":false,
                  "amusegueule":false,
                  "accompagnement":false,
                  "sauce":false,
                  "boisson":true,
                  "confiserie":false}
          },
          "difficulte":{
              "url_para":"dif",
              "values":{"1":true,
                  "2":true,
                  "3":true,
                  "4":true}
          },
          "cout":{
              "url_para":"exp",
              "values":{"1":true,
                  "2":true,
                  "3":true}
          },
          "regime":{
              "url_para":"prt",
              "values":{"1":false,
                  "2":false,
                  "3":false,
                  "4":true}
          },
          "materiel":{
              "url_para":"rct",
              "values":{"1":true,
                  "2":true,
                  "3":true,
                  "4":true,
                  "5":true}
          },
          "temps_de_cuisine":{
              "url_para":"ttlt",
              "values":{"15":true,
                  "30":true,
                  "45":true}
          }
        };
      }
      setUserData(SearchParams);
    }
  });

  useEffect(() => {
    if (!data && "search_url" in f7route.query) {
      console.log("home query search_url");
      console.log(f7route.query);
      fetch(f7route.query["search_url"])
      .then((resp) => resp.json())
      .then((json) => setData(json));
      setInitParam(false);
    };
  });

  useEffect(() => {
    if (UserData && InitParam) {
        var filter_url = "";
        var TypePlatTemp = [];
        for(const value in UserData.type_de_plat.values){
          if (UserData.type_de_plat.values[value]) {
            TypePlatTemp.push(value);
          }
        };
        setTypePlat([...TypePlatTemp]);
        if(TypePlatTemp.length > 0){
          filter_url += "dt="+TypePlatTemp.join(",")+"&";
        }

        var DifficulteTemp = [];
        for(const value in UserData.difficulte.values){
          if (UserData.difficulte.values[value]) {
            DifficulteTemp.push(value);
          }
        };
        setDifficulte([...DifficulteTemp]);
        if(DifficulteTemp.length > 0){
          filter_url += "dif="+DifficulteTemp.join(",")+"&";
        }

        var CoutTemp = [];
        for(const value in UserData.cout.values){
          if (UserData.cout.values[value]) {
            CoutTemp.push(value);
          }
        };
        setCout([...CoutTemp]);
        if(CoutTemp.length > 0){
          filter_url += "exp="+CoutTemp.join(",")+"&";
        }
        
        var RegimeTemp = [];
        for(const value in UserData.regime.values){
          if (UserData.regime.values[value]) {
            RegimeTemp.push(value);
          }
        };
        setRegime([...RegimeTemp]);
        if(RegimeTemp.length > 0){
          filter_url += "prt="+RegimeTemp.join(",")+"&";
        }

        var TempsTemp = [];
        for(const value in UserData.temps_de_cuisine.values){
          if (UserData.temps_de_cuisine.values[value]) {
            TempsTemp.push(value);
          }
        };
        setTemps([...TempsTemp]);
        if(TempsTemp.length > 0){
          filter_url += "ttlt="+TempsTemp.join(",")+"&";
        }        

        var MaterielTemp = [];
        for(const value in UserData.materiel.values){
          if (UserData.materiel.values[value]) {
            MaterielTemp.push(value);
          }
        };
        setMateriel([...MaterielTemp]);
        if(MaterielTemp.length > 0){
          filter_url += "rct="+MaterielTemp.join(",")+"&";
        }        


        fetch("https://ricetta-quotidiana-api.vercel.app/api/v1/menu?"+filter_url)
        .then((resp) => resp.json())
        .then((json) => setData(json));

        setInitSearchUrl(filter_url);
        setInitParam(false);
    };
  });



  const valideRandom = () => {
    fetch("https://ricetta-quotidiana-api.vercel.app/api/v1/menu?nb_random="+NbRandMenu+"&"+InitSearchUrl)
          .then((resp) => resp.json())
          .then((json) => setData(json));
  };

  const valideFilter = () => {
    var filter_url = "";
    if(TypePlat.length > 0){
      filter_url += "dt="+TypePlat.join(",")+"&"
    }
    if(Difficulte.length > 0){
      filter_url += "dif="+Difficulte.join(",")+"&"
    }
    if(Cout.length > 0){
      filter_url += "exp="+Cout.join(",")+"&"
    } 
    if(Regime.length > 0){
      filter_url += "prt="+Regime.join(",")+"&"
    } 
    if(Materiel.length > 0){
      filter_url += "rct="+Materiel.join(",")+"&"
    }        
    if(Temps.length > 0){
      filter_url += "ttlt="+Temps.join(",")+"&"
    } 
    fetch("https://ricetta-quotidiana-api.vercel.app/api/v1/menu?"+filter_url)
          .then((resp) => resp.json())
          .then((json) => setData(json));
  };

  
  const onTypePlatChange= (e) => {
    const value = e.target.value;
    if (e.target.checked) {
      TypePlat.push(value);
    } else {
      TypePlat.splice(TypePlat.indexOf(value), 1);
    }
    setTypePlat([...TypePlat]);
  };

  const onDifficulteChange= (e) => {
    const value = e.target.value;
    if (e.target.checked) {
      Difficulte.push(value);
    } else {
      Difficulte.splice(Difficulte.indexOf(value), 1);
    }
    setDifficulte([...Difficulte]);
  };

  const onCoutChange= (e) => {
    const value = e.target.value;
    if (e.target.checked) {
      Cout.push(value);
    } else {
      Cout.splice(Cout.indexOf(value), 1);
    }
    setCout([...Cout]);
  };

  const onRegimeChange= (e) => {
    const value = e.target.value;
    if (e.target.checked) {
      Regime.push(value);
    } else {
      Regime.splice(Regime.indexOf(value), 1);
    }
    setRegime([...Regime]);
  };

  const onTempsChange= (e) => {
    const value = e.target.value;
    if (e.target.checked) {
      Temps.push(value);
    } else {
      Temps.splice(Temps.indexOf(value), 1);
    }
    setTemps([...Temps]);
  };

  const onMaterielChange= (e) => {
    const value = e.target.value;
    if (e.target.checked) {
      Materiel.push(value);
    } else {
      Materiel.splice(Materiel.indexOf(value), 1);
    }
    setMateriel([...Materiel]);
  };
  
  return(
  <Page name="home">
    {/* Top Navbar */}
    <Navbar large sliding={false}>
      <NavTitle sliding>Ricetta Quotidiana </NavTitle>
      <NavRight sliding>
        <Link iconIos="f7:person_crop_circle" iconAurora="f7:person_crop_circle" iconMd="material:account_circle" href="/setting/" />
      </NavRight>
      <NavTitleLarge>Ricetta Quotidiana</NavTitleLarge>
    </Navbar>

    {/* Page content */}
    <ListeRecette data={data}/>

    {/* Panels Filter */}
    <Panel left cover dark>
          <View>
            <Page>
              <Navbar title="Régler les filtres"/>
              <Button iconIos="f7:arrow_2_circlepath" iconAurora="f7:arrow_2_circlepath" iconMd="material:autorenew" panelClose onClick={() => {setInitParam(true);setUserData({...JSON.parse(localStorage.getItem('mySearchParams'))})
}}>Re-initialiser</Button>
              <Block>
              <List accordionList>
                {/* Type de plat */}
                 <ListItem accordionItem title="Type de plat">
                   <AccordionContent>
                     <Block>
                       <List>
                         <ListItem checkbox title="Entrée" name="demo-checkbox" value="entree" checked={TypePlat.includes('entree')} onChange={(e) => onTypePlatChange(e)}/>
                         <ListItem checkbox title="Plat" name="demo-checkbox" value="platprincipal" checked={TypePlat.includes('platprincipal')} onChange={(e) => onTypePlatChange(e)}/>
                         <ListItem checkbox title="Dessert" name="demo-checkbox" value="dessert" checked={TypePlat.includes('dessert')} onChange={(e) => onTypePlatChange(e)}/>
                         <ListItem checkbox title="Amuse-Gueule" name="demo-checkbox" value="amusegueule" checked={TypePlat.includes('amusegueule')} onChange={(e) => onTypePlatChange(e)}/>
                         <ListItem checkbox title="Accompagnement" name="demo-checkbox" value="accompagnement" checked={TypePlat.includes('accompagnement')} onChange={(e) => onTypePlatChange(e)}/>
                         <ListItem checkbox title="Sauce" name="demo-checkbox" value="sauce" checked={TypePlat.includes('sauce')} onChange={(e) => onTypePlatChange(e)}/>
                         <ListItem checkbox title="Boisson" name="demo-checkbox" value="boisson" checked={TypePlat.includes('boisson')} onChange={(e) => onTypePlatChange(e)}/>
                         <ListItem checkbox title="Confiserie" name="demo-checkbox" value="confiserie" checked={TypePlat.includes('confiserie')} onChange={(e) => onTypePlatChange(e)}/>
                       </List>
                    </Block>
                  </AccordionContent>
                </ListItem>

                    {/* Difficulte */}
                <ListItem accordionItem title="Difficultés">
                   <AccordionContent>
                     <Block>                       
                       <List>
                         <ListItem checkbox title="Très Facile" name="demo-checkbox" value="1" checked={Difficulte.includes('1')} onChange={(e) => onDifficulteChange(e)}/>
                         <ListItem checkbox title="Facile" name="demo-checkbox" value="2" checked={Difficulte.includes('2')} onChange={(e) => onDifficulteChange(e)}/>
                         <ListItem checkbox title="Moyen" name="demo-checkbox" value="3" checked={Difficulte.includes('3')} onChange={(e) => onDifficulteChange(e)}/>
                         <ListItem checkbox title="Difficile" name="demo-checkbox" value="4" checked={Difficulte.includes('4')} onChange={(e) => onDifficulteChange(e)}/>
                       </List>
                    </Block>
                  </AccordionContent>
                </ListItem>

                {/*Cout */}
                <ListItem accordionItem title="Coût">
                   <AccordionContent>
                     <Block>                       
                       <List>
                         <ListItem checkbox title="Bon Marché" name="demo-checkbox" value="1" checked={Cout.includes('1')} onChange={(e) => onCoutChange(e)}/>
                         <ListItem checkbox title="Coût Moyen" name="demo-checkbox" value="2" checked={Cout.includes('2')} onChange={(e) => onCoutChange(e)}/>
                         <ListItem checkbox title="Assez cher" name="demo-checkbox" value="3" checked={Cout.includes('3')} onChange={(e) => onCoutChange(e)}/>
                       </List>
                    </Block>
                  </AccordionContent>
                </ListItem>              

                {/* Regime */}
                <ListItem accordionItem title="Régime Alimentair">
                   <AccordionContent>
                     <Block>                       
                       <List>
                         <ListItem checkbox title="Végétarien" name="demo-checkbox" value="1" checked={Regime.includes('1')} onChange={(e) => onRegimeChange(e)}/>
                         <ListItem checkbox title="Sans gluten" name="demo-checkbox" value="2" checked={Regime.includes('2')} onChange={(e) => onRegimeChange(e)}/>
                         <ListItem checkbox title="Végan" name="demo-checkbox" value="3" checked={Regime.includes('3')} onChange={(e) => onRegimeChange(e)}/>
                         <ListItem checkbox title="Sans Lactose" name="demo-checkbox" value="4" checked={Regime.includes('4')} onChange={(e) => onRegimeChange(e)}/>
                       </List>
                    </Block>
                  </AccordionContent>
                </ListItem>   

                {/* Materiel */}
                <ListItem accordionItem title="Matériel">
                   <AccordionContent>
                     <Block>                       
                       <List>
                         <ListItem checkbox title="Four" name="demo-checkbox" value="1" checked={Materiel.includes('1')} onChange={(e) => onMaterielChange(e)}/>
                         <ListItem checkbox title="Plaques" name="demo-checkbox" value="2" checked={Materiel.includes('2')} onChange={(e) => onMaterielChange(e)}/>
                         <ListItem checkbox title="Sans Cuisson" name="demo-checkbox" value="3" checked={Materiel.includes('3')} onChange={(e) => onMaterielChange(e)}/>
                         <ListItem checkbox title="Micro-ondes" name="demo-checkbox" value="4" checked={Materiel.includes('4')} onChange={(e) => onMaterielChange(e)}/>
                         <ListItem checkbox title="Barbecue/Plancha" name="demo-checkbox" value="5" checked={Materiel.includes('5')} onChange={(e) => onMaterielChange(e)}/>
                       </List>
                    </Block>
                  </AccordionContent>
                </ListItem>   

                {/* Temps */}
                <ListItem accordionItem title="Temps total">
                   <AccordionContent>
                     <Block>                       
                       <List>
                         <ListItem checkbox title="Moins de 15min" name="demo-checkbox" value="15" checked={Temps.includes('15')} onChange={(e) => onTempsChange(e)}/>
                         <ListItem checkbox title="Moins de 30min" name="demo-checkbox" value="30" checked={Temps.includes('30')} onChange={(e) => onTempsChange(e)}/>
                         <ListItem checkbox title="Moins de 45min" name="demo-checkbox" value="45" checked={Temps.includes('45')} onChange={(e) => onTempsChange(e)}/>
                       </List>
                    </Block>
                  </AccordionContent>
                </ListItem>                                  
              </List>

              
              <Button fill raised panelClose onClick={valideFilter}>Valider</Button>
            </Block>
            </Page>
          </View>
    </Panel>

    {/* Popup Menu aleatoire */}
    <Popup id="popup-menu-alea">
        <View>
          <Page>
            <Navbar title="Menu aleatoir">
              <NavRight>
                <Link popupClose>Close</Link>
              </NavRight>
            </Navbar>
            <Block>
              <h1 style={{"color":"#077354","text-align": "center"}}>Nombre de recette(s)</h1>
              <Row>
                <Col width={"25"}></Col>
                <Col width={"40"}> <Stepper min={1} value={NbRandMenu} onStepperChange={setNbRandMenu} large></Stepper> </Col>
                <Col width={"25"}></Col>
              </Row>
              
              <Button popupClose onClick={valideRandom}>Valider</Button>
            </Block>
          </Page>
        </View>
    </Popup>


    {/* Footer */}
    <Block strong className='home-footer'>
      <Row>
        <Col width="50">
          <Button fill raised panelOpen="left">Filtrer</Button>
        </Col>
        <Col width="50">
          <Button fill raised popupOpen="#popup-menu-alea">Menu Aléatoire</Button>
        </Col>
      </Row>
    </Block>
  </Page>
);
}
export default HomePage;