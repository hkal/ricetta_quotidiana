import React from 'react';
import { Page, Navbar, Block, Button } from 'framework7-react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCompactDisc, faCircleNotch, faFan } from '@fortawesome/free-solid-svg-icons'


import "../css/404.css"

const NotFoundPage = () => (
  <Page>
    <Navbar title="Not found" backLink="Back"/>
    <Block>

    <body>
      <div class="mainbox">
        <div class="err">4</div>
        <div class= "error"> <FontAwesomeIcon icon={faCompactDisc} spin/></div>
        <span class="sr-only">Loading...</span>
        <div class="err2">4</div>
        <div class="msg">Hello! la page que vous recherchez à peut-être été déplacé, supprimé ou encore n'a jamais existé. <p>Clic ici: <a href="/"> <div class= "myerrorbutton">Retour à l'accueil</div> </a> Réessayez à partir de là.</p></div>
          </div>      
        </body>
    </Block>
  </Page>
);



export default NotFoundPage;
