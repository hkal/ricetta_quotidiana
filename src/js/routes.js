
import HomePage from '../pages/home.jsx';
import DetailRecette from "../pages/detail_recette";
import NotFoundPage from '../pages/404.jsx';
import SettingPage from '../pages/setting';

var routes = [
  {
    path: '/',
    component: HomePage,
    options: {
      props: {
        search_url: "https://ricetta-quotidiana-api.vercel.app/api/v1/menu",
      },
    }
  },
  {
    // path: '/detail_recette/search_url/:search_url/recette_url/:recette_url/',
    path: '/detail_recette/',
    component: DetailRecette
  },
  {
    path: '/setting/',
    component: SettingPage
  },
  {
    path: '(.*)',
    component: NotFoundPage,
  },
];

export default routes;
