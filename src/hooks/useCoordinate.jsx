

import React, { useState,useEffect } from "react";
import { Capacitor } from '@capacitor/core';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Geolocation } from '@capacitor/geolocation';

export function useCoordinate() {
  const [Coordinates,setCoordinates] = useState([]);




  const findCoordinates = async () => {
    console.log("Coordoniante")

    locationPermission();
    try{
      const coordinate = await Geolocation.getCurrentPosition();
      console.log("Coordoniante taked")
      const newCoordinate = [coordinate, ...Coordinates];
      setCoordinates(newCoordinate);
    } catch{
      console.log("Coordonitae probleme")
    }

  };

  const locationPermission = async () =>{  
    if (Capacitor.isNativePlatform()) {
      AndroidPermissions.checkPermission(AndroidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
          result => {
              if (result.hasPermission) {
                  console.log('ACCESS_COARSE_LOCATION deja ok');
              } else {
                  AndroidPermissions.requestPermission(AndroidPermissions.PERMISSION.ACCESS_COARSE_LOCATION);
              }
          }
      )
  }
  else {
      console.log('Capacitor not detected, this button will do nothing :(')
  }
  
  if (Capacitor.isNativePlatform()) {
    AndroidPermissions.checkPermission(AndroidPermissions.PERMISSION.ACCESS_FINE_LOCATION).then(
        result => {
            if (result.hasPermission) {
              console.log('ACCESS_FINE_LOCATION deja ok');
            } else {
              AndroidPermissions.requestPermission(AndroidPermissions.PERMISSION.ACCESS_FINE_LOCATION);
            }
        }
    )
  }
  else {
    console.log('Capacitor not detected, this button will do nothing :(')
  }
  };
 
  
    return {
      Coordinates,
      findCoordinates,
    };    
}
