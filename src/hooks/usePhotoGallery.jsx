import { useState, useEffect } from 'react';

import { Camera, CameraResultType, CameraSource } from '@capacitor/camera';
import { Filesystem, Directory } from '@capacitor/filesystem';
import { Storage } from '@capacitor/storage';
import { Capacitor } from '@capacitor/core';
import { AndroidPermissions } from '@ionic-native/android-permissions';


export function usePhotoGallery() {

  const [photos,setPhotos] = useState([]);


  const savePicture = async (photo, fileName) => {
    const base64Data = await base64FromPath(photo.webPath);
    const base64DataMarmitton = await base64FromPath(photo.webPath);
    await Filesystem.writeFile({
      path: fileName,
      data: base64Data,
      directory: Directory.Data,
    });
    storePermission();
    try{
      await Filesystem.writeFile({
        path: fileName,
        data: base64Data,
        directory: Directory.Documents,
      });  
    } catch {
        console.log("Permission la famille error");
    }

    console.log("savePictur")
    console.log(Directory.ExternalStorage)
    return {
      base64Data:base64Data,
      filepath: fileName,
      webviewPath: photo.webPath,
    };
  };


  const takePhoto = async () => {
    console.log("TakePhoto")
    const photo = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource.Camera,
      quality: 100,
    });
    console.log("PhotoTaked")
    
    console.log("Savee Photo")
    const fileName = new Date().getSeconds()+'.jpeg';
    const savedFileImage = await savePicture(photo, fileName);
    console.log("Photo saved")
    console.log(savedFileImage.filepath)
    console.log(savedFileImage.webviewPath)
    console.log(savedFileImage.base64Data)
    const newPhotos = [savedFileImage, ...photos];
    setPhotos(newPhotos);
  };

  const storePermission = async () =>{  
    if (Capacitor.isNativePlatform()) {
      AndroidPermissions.checkPermission(AndroidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
          result => {
              if (result.hasPermission) {
                  console.log('READ_EXTERNAL_STORAGE deja ok');
              } else {
                  AndroidPermissions.requestPermission(AndroidPermissions.PERMISSION.READ_EXTERNAL_STORAGE);
              }
          }
      )
  }
  else {
      console.log('Capacitor not detected, this button will do nothing :(')
  }
  
  if (Capacitor.isNativePlatform()) {
    AndroidPermissions.checkPermission(AndroidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(
        result => {
            if (result.hasPermission) {
              console.log('READ_EXTERNAL_STORAGE deja ok');
            } else {
              AndroidPermissions.requestPermission(AndroidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE);
            }
        }
    )
  }
  else {
    console.log('Capacitor not detected, this button will do nothing :(')
  }
  };

 
  
    return {
      photos,
      takePhoto,
    };    
}


export async function base64FromPath(path) {
  const response = await fetch(path);
  const blob = await response.blob();
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onerror = reject;
    reader.onload = () => {
      if (typeof reader.result === 'string') {
        resolve(reader.result);
      } else {
        reject('method did not return a string');
      }
    };
    reader.readAsDataURL(blob);
  });
}